<?php
/**
 * Created by PhpStorm.
 * User: haloa
 * Date: 09/10/2019
 * Time: 20:01
 */

namespace App\Tests;

use Math;
use PHPUnit\Framework\TestCase;


class MathTest extends TestCase
{
    public function testDouble()
    {
         $this->assertEquals(4, \Math::double(2));
    }
}
